<?php

class CideRUseDemise {
  /*-------------------------
  | Developped by Côme Wasik |
  --------------------------*/
  /*-------------
  | Item Diagram |
  ---------------
      Description :
      -------------
        This class allow to interract with a database (with PDO). This without use SQL.
        The only one condition is to put an interface named "iObjetManageable" at a used class.

      Follow the package :
      --------------------
        You can follow the update here : https://github.com/Nolikein/CideRUseDemise

      Attributes :
      -----------
        - $_db : PDO;

      Primary methods :
      --------------------

        + __construct(PDO $initDb)
        - setDb(PDO $newDb)

        + add(object $objet) : bool
        + delete($input, String $nomObjet = '') : int / false
        + get($input, String $nomObjet = '') : object / false
        + getList($input) : array(object) / false
        + update(object $objet) : int / false
        + simpleRequest(String $request, array $args = []) : object / false

      Annexed methods :
      ------------------

        Action on array :
          - clearArray(array $array) : array
          - sparePrimaryKey($listeAttributs, $clePrimaire) : array
          - getTreeOf($nomObjet) : array
          - getFamilyAttr(object $object) : array
        
        Action for SQL requests :
          - generateInsertSqlRequest($nomTable, $listeAttributs) : String
          - generateDeleteSqlRequest($nomTable, $primaryKeys) : String
          - generateGetSqlRequest(String $tableName, array $primaryKeys) : String
          - generateUpdateSqlRequest($nomTable, $listeAttributs, $primaryKeys) : String
          - bindValue($request, $champDonnee, $value)

        Other :
          - getPrimaryKeys(object $object) : array
          - getPrimaryKeysIfAI(object $object) : array
        
      DEBUG NOTE :
      ------------  
        WARNING 001 : All managed class (class with iObjetManageable) must send one array argument in their constructor
        
        NOTE 001 : Protection against injection SQL is okey for me (use of "prepare" method)
        NOTE 002 : Heritage is okey for me
        NOTE 003 : Any method return -1 and other return false if request isn't okey (it's generally due by a bad data with iObjetManageable methods)
  */

  /*-------------
    Attributes :
  -------------*/
  protected $_db;

  /*-------------------
    Primary Methods :
  -------------------*/
  public function __construct(PDO $initDb) {
    $this->setDb($initDb);
  }

  private function setDb(PDO $newDb) { $this->_db = $newDb; }

  public function add(object $objet) : bool {
    // Obtention des informations primaires avec vérification
    $nomTable = $objet->getTableName();
    $listeAttributs = $this->sparePrimaryKey($this->getFamilyAttr($objet), $this->getPrimaryKeysIfAI($objet));

    // Exécution de la requête et remplissage des champs de données
    $request = $this->_db->prepare($this->generateInsertSqlRequest($nomTable, $listeAttributs));
    foreach($listeAttributs as $attribut) {
      $getter = 'get'.ucfirst($attribut);
      $this->bindValue($request, ':' . $attribut, $objet->$getter());
    }

    // Renvoi des informations
    if(!empty($request->execute()))
      return true;
    else
      return false;
  }

  public function delete($input, String $nomObjet = '') : int {
    // Obtention des informations primaires avec vérification
    if( !empty($input) ) {
      if( is_array($input) ) {
        if( !empty($nomObjet)) {
          $nomTable = (new $nomObjet([]))->getTableName();
          $primaryKeysList = $this->getPrimaryKeys(new $nomObjet([]));
          $primaryKeyTab = $input;
          foreach($primaryKeysList as $key) { if(!array_key_exists($key, $primaryKeyTab))
            throw new Exception('Méthode get() - Clé primaire non trouvé : '. $key, 3026);}
        }
        else
          throw new Exception('Méthode delete() : Arg 2 est vide et doit être un String remplit', 3015);
      }
      else if ( is_object($input) ) {
        if( empty($nomObjet) ) {
          $object = $input;
          $nomTable = $object->getTableName();
          $primaryKeysList = $this->getPrimaryKeys($object);
          foreach($primaryKeysList as $keyName) {
            $pKgetmethod = 'get'.ucfirst($keyName);
            $primaryKeyTab[$keyName] = $object->$pKgetmethod();
          }
        }
        else
          throw new Exception('Méthode delete() : Arg 2 doit être vide', 3013);
      }
      else
        throw new Exception('Méthode delete() : Arg 1 doit être string ou objet', 3012);
    }
    else
      throw new Exception('Méthode delete() : Arg 1 ne doit pas être vide', 3011);

    // Exécution de la requête et remplissage des champs de données

    $request = $this->_db->prepare($this->generateDeleteSqlRequest($nomTable, $primaryKeysList));
    foreach($primaryKeysList as $keyName) {
      $this->bindValue($request, ':'.$keyName, $primaryKeyTab[$keyName]);
    }
    
    // Renvoi des informations
    if(!empty($request->execute()))
      return $request->rowCount();
    else
      return -1;
  }

  public function get($input, String $nomClasse = '') {
    // Obtention des informations primaires avec vérification
    if( !empty($input) ) {
      if( is_array($input) ) {
        if( !empty($nomClasse)) {
          $primaryKeyTab = $input;
          if(!class_exists($nomClasse))
            return false;
          $primaryKeysList = $this->getPrimaryKeys(new $nomClasse([]));
          foreach($primaryKeysList as $key) { if(!array_key_exists($key, $primaryKeyTab))
            throw new Exception('Méthode get() - Clé primaire '. $key .' non trouvé dans l\'objet '. $nomClasse, 3026);}
        }
        else
          throw new Exception('Méthode get() : Arg 2 doit être un string remplit', 3025);
      }
      else if ( is_object($input) ) {
        if( empty($nomClasse) ) {
          $object = $input;
          $nomClasse = GetDataClass::getClassName($object);
          $primaryKeysList = $this->getPrimaryKeys($object);
          foreach($primaryKeysList as $keyName) {
            $getPKmethod = 'get'.ucfirst($keyName);
            $primaryKeyTab[$keyName] = $object->$getPKmethod();
          }
        }
        else
          throw new Exception('Méthode get() : Arg 2 doit être vide', 3023);
      }
      else
        throw new Exception('Méthode get() : Arg 1 doit être int ou objet', 3022);
    }
    else
      throw new Exception('Méthode get() : Arg 1 ne doit pas être vide', 3021);

    // Exécution de la requête et remplissage des champs de données
    $tableName = (new $nomClasse([]))->getTableName();
    $request = $this->_db->prepare($this->generateGetSqlRequest($tableName, $primaryKeysList));
    foreach($primaryKeysList as $keyName) {
      $this->bindValue($request, ':'.$keyName, $primaryKeyTab[$keyName]);
    }

    // Renvoi des informations
    if($request->execute()) {
      if(!is_null($data = $request->fetch(PDO::FETCH_ASSOC)))
        return new $nomClasse($data);
      else
        return null;
    }
    else
      return false;
  }

  public function getList($input) {
    // Obtention des informations primaires avec vérification
      if(is_string($input)) {
        if(!empty($input)) {
          $nomClasse = $input;
          if(!class_exists($nomClasse))
            return false;
        }
        else
          throw new Exception('Méthode getList() : Le premier argument ne doit pas être un String vide.', 3033);
      }
      else if(is_object($input)) {
        $nomClasse = GetDataClass::getClassName($input);
      }
      else
        throw new Exception('Méthode getList() : Le premier argument doit être un String ou un objet.', 3031);
 
      $objectList = [];
      
      // Exécution de la requête et remplissage des champs de données
      $preparedRequest = 'SELECT * FROM ' . (new $nomClasse([]))->getTableName() . ';';
      if($request = $this->_db->query($preparedRequest)) {
        while($data = $request->fetch(PDO::FETCH_ASSOC)) {
            $objectList[] = new $nomClasse($data);
        }

        // Renvoi des informations
        if(!empty($objectList))
          return $objectList;
        else
          return null;
      }
      else
        return false;
  }

  public function update(object $object) : int {
    // Obtention des informations primaires avec vérification
    $nomTable = $object->getTableName();
    $primaryKeys = $this->getPrimaryKeys($object);
    $listeAttributs = $this->sparePrimaryKey($this->getFamilyAttr($object), $this->getPrimaryKeysIfAI($object));

    // Exécution de la requête et remplissage des champs de données
    $request = $this->_db->prepare($this->generateUpdateSqlRequest($nomTable, $listeAttributs, $primaryKeys));
    foreach($listeAttributs as $attribut) {
      $champDonnee = ':' . $attribut;
      $getter = 'get'.ucfirst($attribut);

      $this->bindValue($request, $champDonnee, $object->$getter());
    }
    foreach($primaryKeys as $primaryKey) {
      $champDonneeCle = ':' . $primaryKey;
      $getPKmethod = 'get'.ucfirst($primaryKey);
      $this->bindValue($request, $champDonneeCle, $object->$getPKmethod());
    }

    // Renvoi des informations
    if(!empty($request->execute()))
      return $request->rowCount();
    else
      return -1;
  }

  public function simpleRequest(String $userRequest, array $args = []) : bool {
    $request = $this->_db->prepare($userRequest);
    if(!empty($args)) {
      if($request->execute($args))
        return true;
      else
        return false;
    }
    else {
      if($request->execute())
        return true;
      else
        return false;
    }
  }
  
  /*---------------
    Annex methods
  ---------------*/

  // Opérations sur les arrays
  // -------------------------

  private function clearArray(array $array) {
    foreach($array as $element) {
            if($element != null)
                    $return[] = $element;
    }
    return $return;
  } 

  private function sparePrimaryKey($listeAttributs, $clesPrimaire) {
    foreach($clesPrimaire as $clePrimaire) {
      foreach($listeAttributs as &$attribut) {
        if($attribut == $clePrimaire) {
            $attribut = null;
        }
      }
    }
    return $this->clearArray($listeAttributs);
  }


  private function getTreeOf($nomObjet) {
    if(!empty($parents = GetDataClass::getParentList($nomObjet))) {
      $parentsTableName = [];
      foreach($parents as $parent) {
        $parentsTableName[] = $parent;
      }
      return array_merge([$nomObjet], $parentsTableName);
    }
    else
      return [$nomObjet];
  }

  private function getFamilyAttr(object $objet) {
    $attributDeLobjet = GetDataClass::getAttributesName($objet);
    if(!empty(GetDataClass::getParentList($objet))) {
      return array_merge($attributDeLobjet, GetDataClass::getParentsAttributes($objet));
    }
    else
      return $attributDeLobjet;
  }

  // Opérations pour les requêtes SQL :
  // ----------------------------------

  private function generateInsertSqlRequest(String $nomTable, array $listeAttributs) {
    $preparedRequest = 'INSERT INTO ' . $nomTable . ' (';
    foreach($listeAttributs as $attribut) {
        $preparedRequest .= $attribut . ', ';
    }

    // Mise en place des noms de chaque données
    $preparedRequest = rtrim($preparedRequest, ', ');
    $preparedRequest .= ') VALUES (';

    // Mise en place des champs de chaque données
    foreach($listeAttributs as $attribut) {
        $champDonnee = ':' . $attribut;
        $preparedRequest .= $champDonnee . ', ';
    }
    $preparedRequest = rtrim($preparedRequest, ', ');
    $preparedRequest .= ');';
    return $preparedRequest;
  }

  private function generateDeleteSqlRequest(String $nomTable, array $primaryKeys) {
    $preparedRequest = 'DELETE FROM ' . $nomTable . ' WHERE ';
    foreach($primaryKeys as $primaryKey) {
      $preparedRequest .= $primaryKey . ' = :' . $primaryKey . ' && ';
    }
    $preparedRequest = rtrim($preparedRequest, '& ');
    $preparedRequest .= ';';
    return $preparedRequest;
  }

  private function generateGetSqlRequest(String $tableName, array $primaryKeys) {
    $preparedRequest = 'SELECT * FROM ' . $tableName . ' WHERE ';
    foreach($primaryKeys as $primaryKey) {
      $preparedRequest .= $primaryKey . ' = :' . $primaryKey . ' && ';
    }
    $preparedRequest = rtrim($preparedRequest, '& ');
    $preparedRequest .= ';';
    return $preparedRequest;
  }

  private function generateUpdateSqlRequest(String $nomTable, array $listeAttributs, array $primaryKeys) {
    $preparedRequest = 'UPDATE ' . $nomTable . ' SET ';
    // Mise en place des noms et champs de chaque données
    foreach($listeAttributs as $attribut) {
      $champDonnee = ':' . $attribut;
      $preparedRequest .= $attribut . ' = ' . $champDonnee . ', ';
    }
    $preparedRequest = rtrim($preparedRequest, ', ');
    $preparedRequest .= ' WHERE ';
    foreach($primaryKeys as $primaryKey) {
      $preparedRequest .= $primaryKey . ' = :' . $primaryKey . ' && ';
    }
    $preparedRequest = rtrim($preparedRequest, '& ');
    $preparedRequest .= ';';
    return $preparedRequest;
  }

  protected function bindValue($request, $champDonnee, $value) {
    if(is_int($value))
      $request->bindValue($champDonnee, $value, PDO::PARAM_INT);
    else if(is_null($value))
      $request->bindValue($champDonnee, $value, PDO::PARAM_NULL);
    else
      $request->bindValue($champDonnee, $value);
  }

  // Autres
  // ------
  private function getPrimaryKeys($objet) {
    $pk = $objet->getPrimaryKey();
    if(is_array($pk))
      return $pk;
    else
      return [$pk];
  }

  private function getPrimaryKeysIfAI($objet) {
    if($objet->PKIsAutoIncremented())
      $pk = $objet->getPrimaryKey();
    else
      $pk = '';

    if(is_array($pk))
      return $pk;
    else
      return [$pk];
  }
}