<?php

class GetDataClass {
  /*-------------------------
  | Developped by Côme Wasik |
  --------------------------*/
  /*-------------
  | Item Diagram |
  ---------------
      Description :
      -------------
        This class allow to get data from others class. It's more esay to use it.
        The class "CideRUseDemise" need this class.

      Follow the package :
      --------------------
        You can follow the update here : https://github.com/Nolikein/CideRUseDemise

      Méthodes :
      ----------

        +s getNomAttributs($input) : array[String]
        +s getParentList($input) : array[String]
        +s getParentsAttributs($objet) : array[String]
        +s getClassName($object) :  String

    */
  
    public static function getAttributesName($input) {
        try {
          if(is_string($input))
            $rechercheClasse = new ReflectionClass($input);
          else if(is_object($input)) {
            $rechercheClasse = new ReflectionObject($input);
          }

          foreach($rechercheClasse->getProperties() as $attribut) {
            $listeAttributs[] = $attribut->getName();
          }
          if(!empty($listeAttributs))
            return $listeAttributs;
          else
            throw new ReflectionException();
        } catch(ReflectionException $e) {
          return null;
        }
      }
    
      public static function getParentList($input) {
        try {
          if(is_string($input)) {
            $rechercheClasse = new ReflectionClass($input);
          }
          else if(is_object($input))
            $rechercheClasse = new ReflectionObject($input);
          else
            return false;
      
          while ($parent = $rechercheClasse->getParentClass()) {
            $parents[] = $parent->getName();
            $rechercheClasse = $parent;
          }
          if(!empty($parents))
            return $parents;
          else
            throw new ReflectionException();
        } catch(ReflectionException $e) {
          return null;
        }
      }
    
      public static function getParentsAttributes($objet) {
        $listeAttributs = [];
        foreach(DataClass::getParentList($objet) as $parent) {
          if($attributsSupplementaires = DataClass::getNomAttributs($parent))
            $listeAttributs = array_merge($listeAttributs, $attributsSupplementaires);
        }
        return $listeAttributs;
      }

      public static function getClassName($object) {
        try {
          $rechercheClasse = new ReflectionObject($object);
          return $rechercheClasse->getShortName();
        } catch(ReflectionException $e) {
          return null;
        }
      }
}