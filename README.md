# CideRUseDemise
It's my fisrt package on github.

This package is an ORM (Object-Relational Mapping) with CRUD commands for Php. It's use to make easielly SQL request without write any SQL code.

There is just three constraints. You must :
  + Include CideRUseDemise.ini.php in your project.
  + Implement at all objects controlled by database the interface named "iManageableObject"
  + Verify that all attributes of your object correspond at all attributes of your table. It's ORM you know.
  
Bonus :
  + You can make a child of "CideRUseDemise" to add more SQL request and keep an Self-contained CRUD.
  + You can make a child of "DbMainStarter" for each databases.

I like use an ORM CRUD, and I was make it in my studies to use SQL request easielly.
Enjoy
